package main

import (
	"bytes"
	"fmt"
	"log"
	"os"
	"path"
	"path/filepath"
	"strings"
	"text/template"

	"gitlab.com/bboehmke/iot-pi/apps"
)

func main() {
	if len(os.Args) < 3 {
		log.Fatal("Missing path and/or architecture to destination file")
	}

	arch := os.Args[1]

	file, err := os.Create(os.Args[2])
	if err != nil {
		log.Fatalf("Failed to create file: %v", err)
	}
	defer file.Close()

	fmt.Fprintf(file, "#!/bin/sh\nset -e\n")

	log.Printf("%d apps to download", len(apps.AppDefs))

	var kernelModules []string
	for key, data := range apps.AppDefs {
		log.Printf("[%s] version %s of %s", key, data.Meta.Version, data.Meta.Name)
		kernelModules = append(kernelModules, data.Install.Modules...)

		fmt.Fprintf(file, "\ncolour_echo \"install %s\" \"$Cyan\"\n", key)
		if len(data.Install.Apk) > 0 {
			tpl, err := template.New("").Parse(strings.Join(data.Install.Apk, " "))
			if err != nil {
				log.Fatalf("[%s] Invalid template for APK packages %s: %v",
					key, strings.Join(data.Install.Apk, " "), err)
			}

			fmt.Fprintf(file, "chroot_exec apk add --no-cache ")
			err = tpl.Execute(file, data)
			if err != nil {
				log.Fatalf("[%s] failed to execute: %v", key, err)
			}
			fmt.Fprintf(file, "\n")
		}

		if len(data.Install.Pip) > 0 {
			tpl, err := template.New("").Parse(strings.Join(data.Install.Pip, " "))
			if err != nil {
				log.Fatalf("[%s] Invalid template for PIP packages %s: %v",
					key, strings.Join(data.Install.Pip, " "), err)
			}

			fmt.Fprintf(file, "chroot_exec pip3 install ")
			err = tpl.Execute(file, data)
			if err != nil {
				log.Fatalf("[%s] failed to execute: %v", key, err)
			}
			fmt.Fprintf(file, "\n")
		}

		if len(data.Install.Download) > 0 {
			item, ok := data.Install.Download[arch]
			if !ok {
				log.Fatalf("[%s] no download for arch \"%s\": %v", key, arch, err)
			}

			tpl, err := template.New("").Parse(item.Url)
			if err != nil {
				log.Fatalf("[%s] Invalid template for URL packages %s: %v",
					key, item.Url, err)
			}

			var url bytes.Buffer
			err = tpl.Execute(&url, data)
			if err != nil {
				log.Fatalf("[%s] failed to execute: %v", key, err)
			}

			tmpFile := filepath.Join("/tmp/", key, path.Base(url.String()))
			fmt.Fprintf(file, "mkdir -p %s\n", filepath.Dir(tmpFile))
			fmt.Fprintf(file, "mkdir -p ${ROOTFS_PATH}%s\n", data.Meta.AppDir)
			fmt.Fprintf(file, "wget -O %s %s\n", tmpFile, url.String())
			fmt.Fprintf(file, "echo \"%s *%s\" | sha256sum -b -c -\n", item.Sha256, tmpFile)
			fmt.Fprintf(file, "tar -xf %s -C ${ROOTFS_PATH}%s\n", tmpFile, data.Meta.AppDir)
		}
		fmt.Fprintf(file, "\n")
	}

	if len(kernelModules) > 0 {
		for _, module := range kernelModules {
			fmt.Fprintf(file, "echo \"%s\" >> ${ROOTFS_PATH}/etc/modules\n", module)
		}
		fmt.Fprintf(file, "echo \"\" >> ${ROOTFS_PATH}/etc/modules\n")

		fmt.Fprintf(file, "\n")
	}
}
