# IoT-Pi

Iot-Pi is a minimal image for the Raspberry Pi that provides multiple IoT 
applications. A web interface to enable/disable and configure the applications
and the operating system makes it easy to set up.

Features:
 * read-only root file system (protect SD card)
 * update running system (manual or automatic)
 * support for multiple applications (option to add more later)
 * simple web interface:
     * manage state (start/stop or enable/disable autostart)
     * edit application config
     * view application log
     * configure system
     * update system
     * password protected (Default: `iot-pi`)

## Provided applications

IoT-Pi provides multiple applications that are by default disabled and are not 
starting automatically. To use them create the necessary configuration and 
enable the application in the web interface.

| Application                                 | Description                                                                                              |
|---------------------------------------------|----------------------------------------------------------------------------------------------------------|
| [EVCC](https://github.com/evcc-io/evcc)     | Extensible EV Charge Controller with PV integration                                                      |
| [Mosquitto](https://mosquitto.org/)         | An open source MQTT broker                                                                               |
| [MQTT IO](https://github.com/flyte/mqtt-io) | Exposes general purpose inputs and outputs (GPIO), hardware sensors and serial devices to an MQTT server |

If you are interested in additional applications feel free to create an 
[Issue](https://gitlab.com/bboehmke/iot-pi/-/issues) or directly create a Merge 
Request.

## Getting started

1. Download the `iot-pi_armv7.img.gz` from the last 
[Release](https://gitlab.com/bboehmke/iot-pi/-/releases)
2. Flash the image to an empty SD card
3. Insert the SD into a Raspberry Pi (at least a Pi 2)
4. Connect the Raspberry Pi to the network (only LAN supported) and to power 

> By default, the network is configured via DHCP but can be set to a static 
> one in the web interface.

5. Access the web interface via the IP address that got assigned via DHCP
6. Login with `root` as username and `iot-pi` as password

> The password should be changed for a productive system

7. Configure and enable/start the applications you want to use