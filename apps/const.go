package apps

// AppBinBaseDir to store applications
const AppBinBaseDir = "/opt/iot-pi/"

// AppDataBaseDir to store application data
const AppDataBaseDir = "/data/iot-pi/"

// AppConfFile to store application configuration
const AppConfFile = AppDataBaseDir + "apps.yaml"

// UpdateImageFile used to store update before flash
const UpdateImageFile = AppDataBaseDir + "image.gz"

// UpdateImageNameFile used to store original name of image file
const UpdateImageNameFile = AppDataBaseDir + "image_filename"
