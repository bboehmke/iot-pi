package apps

import (
	"fmt"
	"os"
	"path/filepath"
	"sync"

	"gopkg.in/yaml.v3"
)

// Apps holds the global AppManager instance
var Apps *AppManager

// AppManager handles the applications managed by IoT-Pi
type AppManager struct {
	apps  map[string]*App
	mutex sync.RWMutex
}

// InitializeAppManager and start enabled applications
func InitializeAppManager() {
	Apps = &AppManager{
		apps: make(map[string]*App, len(AppDefs)),
	}

	for key, def := range AppDefs {
		Apps.apps[key] = NewApp(def)
	}
	Apps.loadConf()

	// start enabled apps
	for _, app := range Apps.apps {
		if app.Conf.AutostartEnabled {
			app.AutostartStart()
		}
	}
}

// Apps returns a list with all apps managed by IoT-Pi
func (m *AppManager) Apps() map[string]*App {
	m.mutex.RLock()
	defer m.mutex.RUnlock()

	return m.apps // TODO race condition
}

// GetApp returns the app with the given name
func (m *AppManager) GetApp(key string) *App {
	m.mutex.RLock()
	defer m.mutex.RUnlock()

	return m.apps[key]
}

// SetAutostart state of application
func (m *AppManager) SetAutostart(key string, enabled bool) error {
	m.mutex.RLock()
	defer m.mutex.RUnlock()

	app, ok := m.apps[key]
	if !ok {
		return fmt.Errorf("app %s not found", key)
	}

	app.Conf.AutostartEnabled = enabled

	return m.saveConf()
}

// loadConf for applications from file
func (m *AppManager) loadConf() {
	file, err := os.Open(AppConfFile)
	if err != nil {
		return // no config -> skip load
	}
	defer file.Close()

	var conf map[string]AppConfig
	err = yaml.NewDecoder(file).Decode(&conf)
	if err != nil {
		return // invalid config -> skip load
	}

	m.mutex.Lock()
	defer m.mutex.Unlock()
	for k, c := range conf {
		if app, ok := m.apps[k]; ok {
			app.Conf = c
		}
	}
}

// saveConf for applications to file
func (m *AppManager) saveConf() error {
	conf := make(map[string]AppConfig)
	for key, app := range m.apps {
		conf[key] = app.Conf
	}

	err := os.MkdirAll(filepath.Dir(AppConfFile), os.ModePerm)
	if err != nil {
		return fmt.Errorf("failed to create app config dir: %w", err)
	}

	file, err := os.Create(AppConfFile)
	if err != nil {
		return fmt.Errorf("failed to open app config file: %w", err)
	}
	defer file.Close()

	err = yaml.NewEncoder(file).Encode(&conf)
	if err != nil {
		return fmt.Errorf("failed to encode app config: %w", err)
	}

	return nil
}
