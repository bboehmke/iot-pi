package apps

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLogBuffer_Add(t *testing.T) {
	ass := assert.New(t)
	buffer := NewLogBuffer(5)

	for i := 1; i <= 3; i++ {
		buffer.Add(fmt.Sprintf("%d", i))
	}

	ass.Equal([]string{"1", "2", "3"}, buffer.Lines())

	for i := 4; i <= 9; i++ {
		buffer.Add(fmt.Sprintf("%d", i))
	}

	ass.Equal([]string{"5", "6", "7", "8", "9"}, buffer.Lines())
}
