package apps

import (
	"strings"
	"sync"
)

// LogBuffer ring buffer that holds the last x lines
type LogBuffer struct {
	buffer []string
	size   int
	end    int
	mutex  sync.RWMutex
}

// NewLogBuffer creates a new log buffer with the given size
func NewLogBuffer(size int) *LogBuffer {
	return &LogBuffer{
		buffer: make([]string, size),
		size:   0,
		end:    0,
	}
}

// Add line to buffer
func (b *LogBuffer) Add(line string) {
	b.mutex.Lock()
	defer b.mutex.Unlock()

	if b.size < len(b.buffer) {
		b.size += 1
	}

	if b.end >= len(b.buffer) {
		b.end = 0
	}

	b.buffer[b.end] = line
	b.end += 1
}

// Lines returns all lines stored in buffer
func (b *LogBuffer) Lines() []string {
	b.mutex.RLock()
	defer b.mutex.RUnlock()

	if b.size < len(b.buffer) {
		return b.buffer[:b.size]
	}
	if b.end >= len(b.buffer) {
		return b.buffer
	}
	return append(b.buffer[b.end:], b.buffer[:b.end]...)
}

// String returns content of buffer as string
func (b *LogBuffer) String() string {
	return strings.Join(b.Lines(), "\n")
}
