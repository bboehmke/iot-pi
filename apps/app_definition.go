package apps

import (
	"embed"
	"io/fs"
	"path/filepath"
	"strings"
	"time"

	"gopkg.in/yaml.v3"
)

// AppDefs contains a map with all provided applications
var AppDefs = make(map[string]AppDef)

//go:embed defs/*
var assets embed.FS

// load app definition
func init() {
	// load apps
	err := fs.WalkDir(assets, "defs", func(path string, d fs.DirEntry, err error) error {
		if err != nil {
			panic(err)
		}

		// ignore directories
		if d.IsDir() {
			return nil
		}

		// open config file
		file, err := assets.Open(path)
		if err != nil {
			panic(err)
		}
		defer file.Close()

		// and pare content
		var conf AppDef
		err = yaml.NewDecoder(file).Decode(&conf)
		if err != nil {
			panic(err)
		}

		// app key = file name without extension
		key := filepath.Base(path)
		key = key[:strings.IndexByte(key, '.')]

		// add some calculated values
		conf.Meta.Key = key
		conf.Meta.AppDir = filepath.Join(AppBinBaseDir, key)
		conf.Meta.DataDir = filepath.Join(AppDataBaseDir, key)
		if conf.Meta.Name == "" {
			conf.Meta.Name = key
		}

		AppDefs[key] = conf

		return nil
	})
	if err != nil {
		panic(err)
	}
}

// DownloadItem that defines a file to download
type DownloadItem struct {
	Url    string
	Sha256 string
}

// AppDef represents the definition  of a single application
type AppDef struct {
	Meta struct {
		Key     string `yaml:"-"`
		AppDir  string `yaml:"-"`
		DataDir string `yaml:"-"`

		Name             string
		Version          string
		Description      string
		ProjectUrl       string `yaml:"project_url"`
		DocumentationUrl string `yaml:"documentation_url"`
	}

	Install struct {
		Pip      []string
		Apk      []string
		Download map[string]DownloadItem
		Modules  []string // list of kernel modules to load
	}

	Run struct {
		Command        string
		Dir            string
		AutostartDelay time.Duration `yaml:"autostart_delay"`
	}

	Config map[string]struct {
		Name        string
		DistExample string `yaml:"dist_example"`
	}
}
