package apps

import (
	"bufio"
	"bytes"
	"fmt"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"sync"
	"syscall"
	"text/template"
	"time"
)

// AppConfig contains configuration for an application
type AppConfig struct {
	// AutostartEnabled is true if the application should be started on startup
	AutostartEnabled bool `yaml:"autostart_enabled"`
}

// ConfigFile for an application
type ConfigFile struct {
	// Name of configuration file
	Name string
	// Path to configuration file
	Path string
	// Example content for configuration file
	Example string

	mutex sync.RWMutex
}

// Read configuration file from filesystem
func (c *ConfigFile) Read() string {
	c.mutex.RLock()
	defer c.mutex.RUnlock()

	content, err := os.ReadFile(c.Path)
	if err != nil {
		log.Printf("failed to read config: %v", err)
		return ""
	}
	return string(content)
}

// Write configuration file to file system
func (c *ConfigFile) Write(content string) error {
	c.mutex.Lock()
	defer c.mutex.Unlock()

	// ensure directory for config file exist
	err := os.MkdirAll(filepath.Dir(c.Path), os.ModePerm)
	if err != nil {
		return fmt.Errorf("failed to create config dir: %w", err)
	}

	err = os.WriteFile(c.Path, []byte(content), os.ModePerm)
	if err != nil {
		return fmt.Errorf("failed to write config: %w", err)
	}
	return nil
}

// App that is managed by IoT-Pi
type App struct {
	// Def defines the application
	Def AppDef
	// Conf contains the actual application config
	Conf AppConfig
	// ConfigFiles for application
	ConfigFiles map[string]*ConfigFile

	// log contains last x lines of command log
	log *LogBuffer
	// cmd holds the running application process
	cmd *exec.Cmd

	autostartTimer *time.Timer

	mutex sync.Mutex
}

// NewApp creates a new application instance from a definition
func NewApp(def AppDef) *App {
	app := App{
		Def: def,
		log: NewLogBuffer(500),
	}

	// create config files from definition
	app.ConfigFiles = make(map[string]*ConfigFile, len(def.Config))
	for key, conf := range def.Config {
		app.ConfigFiles[key] = &ConfigFile{
			Name: conf.Name,
			Path: filepath.Join(AppDataBaseDir, def.Meta.Key, conf.Name),
		}

		// load examples from file
		exampleFile := app.handleTpl(conf.DistExample)
		if exampleFile != "" {
			example, err := os.ReadFile(exampleFile)
			if err != nil {
				log.Printf("%s: failed to load example config: %v", def.Meta.Key, err)
			} else {
				app.ConfigFiles[key].Example = string(example)
			}
		}
	}

	return &app
}

// isRunning returns true if the application is running (no mutex only internal use)
func (a *App) isRunning() bool {
	// no process -> not running
	if a.cmd == nil || a.cmd.Process == nil {
		return false
	}

	// process exists but no result -> running
	if a.cmd.ProcessState == nil {
		return true
	}

	// process result present -> not running
	return false
}

// IsRunning returns true if the application is running
func (a *App) IsRunning() bool {
	a.mutex.Lock()
	defer a.mutex.Unlock()

	return a.isRunning()
}

// handleTpl and return the result
func (a *App) handleTpl(tpl string) string {
	if tpl == "" {
		return ""
	}

	t, err := template.New("").Parse(tpl)
	if err != nil {
		log.Printf("failed to parse template \"%s\": %v", tpl, err)
		return ""
	}

	var buf bytes.Buffer
	err = t.Execute(&buf, a)
	if err != nil {
		log.Printf("failed to exec template \"%s\": %v", tpl, err)
		return ""
	}

	return buf.String()
}

// AutostartStart application
func (a *App) AutostartStart() {
	if a.IsRunning() {
		return
	}

	a.mutex.Lock()
	defer a.mutex.Unlock()

	// disable running autostart timer
	if a.autostartTimer != nil {
		a.autostartTimer.Stop()
		a.autostartTimer = nil
	}

	log.Printf("%s: automatic start in %v", a.Def.Meta.Key, a.Def.Run.AutostartDelay)
	a.autostartTimer = time.NewTimer(a.Def.Run.AutostartDelay)
	go func() {
		select {
		case <-a.autostartTimer.C:
			a.mutex.Lock()
			defer a.mutex.Unlock()

			log.Printf("%s: automatic start", a.Def.Meta.Key)
			go func() {
				err := a.Start()
				if err != nil {
					log.Printf("%s: failed to autostart: %v", a.Def.Meta.Key, err)
				}
			}()
			a.autostartTimer = nil

		case <-time.After(a.Def.Run.AutostartDelay + time.Second*2):
			// timer not triggered -> stop goroutine
		}
	}()
}

// Start application
func (a *App) Start() error {
	a.mutex.Lock()
	defer a.mutex.Unlock()

	if a.isRunning() {
		return nil
	}

	// disable running autostart timer
	if a.autostartTimer != nil {
		a.autostartTimer.Stop()
		a.autostartTimer = nil
	}

	// get command from definition
	commandSplit := strings.Split(a.handleTpl(a.Def.Run.Command), " ")

	a.cmd = exec.Command(commandSplit[0], commandSplit[1:]...)
	a.cmd.Dir = a.handleTpl(a.Def.Run.Dir)
	a.cmd.SysProcAttr = &syscall.SysProcAttr{Setpgid: true}

	// forward stdout and stderr to log buffer
	stdout, _ := a.cmd.StdoutPipe()
	go func() {
		scanner := bufio.NewScanner(stdout)
		for scanner.Scan() {
			a.log.Add(scanner.Text())
		}
	}()
	stderr, _ := a.cmd.StderrPipe()
	go func() {
		scanner := bufio.NewScanner(stderr)
		for scanner.Scan() {
			a.log.Add(scanner.Text())
		}
	}()

	// start the process
	err := a.cmd.Start()
	if err != nil {
		return fmt.Errorf("failed to start %s: %w", a.Def.Meta.Key, err)
	}

	go func() {
		err := a.cmd.Wait()
		if err != nil {
			log.Printf("app %s stopped: %v", a.Def.Meta.Key, err)
		}
	}()

	return nil
}

// Stop the application
func (a *App) Stop() error {
	a.mutex.Lock()
	defer a.mutex.Unlock()

	// disable running autostart timer
	if a.autostartTimer != nil {
		a.autostartTimer.Stop()
		a.autostartTimer = nil
	}

	if !a.isRunning() {
		return nil
	}

	// try sigterm first
	_ = a.cmd.Process.Signal(syscall.SIGTERM)

	// wait up to 10 seconds for stop
	start := time.Now()
	for {
		time.Sleep(time.Millisecond * 100)
		if !a.isRunning() {
			return nil
		}
		if time.Since(start) > time.Second*10 {
			break
		}
	}
	log.Printf("terminate of %s timeout -> killing process", a.Def.Meta.Key)

	// send sigkill to process
	err := a.cmd.Process.Kill()
	if err != nil {
		return fmt.Errorf("failed to kill %s: %w", a.Def.Meta.Key, err)
	}
	return nil
}

// Log of application
func (a *App) Log() string {
	return a.log.String()
}
