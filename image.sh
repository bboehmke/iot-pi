#!/bin/sh
set -e

# disable swclock to wait for startup
chroot_exec rc-update delete ab_clock default

colour_echo "install go to build iot-pi"
apk add --no-cache go

colour_echo "install some base packages"
chroot_exec apk add --no-cache python3 py3-pip py3-wheel

cd ${INPUT_PATH}

colour_echo "build iot-pi and add to image"
GOARCH=arm GOARM=5 go build -v -o ${ROOTFS_PATH}/usr/bin/iot-pi -ldflags "-s -w -X gitlab.com/bboehmke/iot-pi/version.Version=${CI_COMMIT_TAG:-'0.0.0'}" ./main.go
cp ${INPUT_PATH}/service_init.sh ${ROOTFS_PATH}/etc/init.d/iot-pi
chroot_exec rc-update add iot-pi default

colour_echo "install applications"
go run ./cmd/install-gen/main.go ${ARCH} /tmp/app_install.sh
chmod +x /tmp/app_install.sh

. /tmp/app_install.sh

colour_echo "cleanup"
rm -r ${ROOTFS_PATH}/data/root/.cache
