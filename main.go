package main

import (
	"flag"
	"log"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/bboehmke/iot-pi/apps"
	"gitlab.com/bboehmke/iot-pi/web"
	alpine_builder "gitlab.com/raspi-alpine/go-raspi-alpine"
)

// define flags
var enableAuth = flag.Bool("enable-auth", false, "Enable basic auth for web interface")
var resetBootCounter = flag.Bool("reset-boot-counter", false, "Reset UBoot boot counter")
var listenAddress = flag.String("listen-addr", ":8080", "Address to listen for web interface")

func main() {
	flag.Parse()

	// wait for valid time
	for time.Now().Year() < 2000 {
		time.Sleep(time.Second)
	}

	// initialize application manager and automatically start enabled apps
	apps.InitializeAppManager()

	// definer router
	router := gin.New()
	router.Use(gin.Logger(), gin.Recovery())
	router.SetHTMLTemplate(web.Tpl)

	// enable basic auth if flag is set
	if *enableAuth {
		router.Use(web.BasicAuth)
	}

	// register handler for web pages
	web.RegisterApps(router)
	web.RegisterSystem(router)
	web.RegisterUpdate(router)

	// redirect root to application page
	router.GET("/", func(c *gin.Context) {
		c.Redirect(http.StatusTemporaryRedirect, "/apps")
	})

	// reset boot counter to mark boot as successful
	if *resetBootCounter {
		err := alpine_builder.UBootResetCounter()
		if err != nil {
			log.Fatalf("failed to reset boot counter: %v", err)
		}
	}

	// start web server
	err := router.Run(*listenAddress)
	if err != nil {
		log.Fatal(err)
	}
}
