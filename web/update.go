package web

import (
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/bboehmke/iot-pi/apps"
	"gitlab.com/raspi-alpine/go-raspi-alpine"
)

func RegisterUpdate(router gin.IRouter) {
	group := router.Group("/update", render)

	group.GET("/", getUpdate)
	group.POST("/", postUpdate, simpleTpl("Update", "update"))
	group.GET("/confirm", getUpdateConfirm)
	group.POST("/confirm", postUpdateConfirm, getUpdateConfirm)
}

func getUpdate(ctx *gin.Context) {
	ctx.Set("title", "Update")
	ctx.Set("tpl", "update")

	updateVersion, _, err := getAvailableUpdate()
	if err != nil {
		ctx.Set("err", err)
	} else {
		vars := ctx.MustGet("vars").(map[string]interface{})
		vars["UpdateVersion"] = updateVersion
	}
}

func postUpdate(ctx *gin.Context) {
	switch ctx.PostForm("action") {
	case "update_manual":
		uploadFile, fileHeader, err := ctx.Request.FormFile("file")
		if err != nil {
			ctx.Set("err", fmt.Errorf("failed to get image file: %w", err))
			return
		}
		defer uploadFile.Close()

		_ = os.MkdirAll(filepath.Dir(apps.UpdateImageFile), os.ModePerm)
		_ = os.Remove(apps.UpdateImageFile)

		imgFile, err := os.Create(apps.UpdateImageFile)
		if err != nil {
			ctx.Set("err", fmt.Errorf("failed to create image file: %w", err))
			return
		}
		defer imgFile.Close()

		_, err = io.Copy(imgFile, uploadFile)
		if err != nil {
			ctx.Set("err", fmt.Errorf("failed to upload image file: %w", err))
			return
		}

		_ = ioutil.WriteFile(apps.UpdateImageNameFile, []byte(fileHeader.Filename), os.ModePerm)

		// TODO sync multiple requests
		ctx.Redirect(http.StatusFound, "/update/confirm")
		ctx.Abort()

	case "update_automatic":
		updateVersion, updateUrl, err := getAvailableUpdate()
		if err != nil {
			ctx.Set("err", err)
			return
		}

		if updateVersion != ctx.PostForm("version") {
			ctx.Set("err", fmt.Errorf("invalid version %s given", ctx.PostForm("version")))
			return
		}

		response, err := http.Get(updateUrl)
		if err != nil {
			ctx.Set("err", fmt.Errorf("failed to get update for version %s: %w", updateVersion, err))
			return
		}
		defer response.Body.Close()

		_ = os.MkdirAll(filepath.Dir(apps.UpdateImageFile), os.ModePerm)
		_ = os.Remove(apps.UpdateImageFile)

		imgFile, err := os.Create(apps.UpdateImageFile)
		if err != nil {
			ctx.Set("err", fmt.Errorf("failed to create image file: %w", err))
			return
		}
		defer imgFile.Close()

		hasher := sha256.New()
		copyReader := io.TeeReader(response.Body, imgFile)
		_, err = io.Copy(hasher, copyReader)
		if err != nil {
			ctx.Set("err", fmt.Errorf("failed to download image file: %w", err))
			return
		}
		checksumFile := hex.EncodeToString(hasher.Sum(nil))

		hashResp, err := http.Get(updateUrl + ".sha256")
		if err != nil {
			ctx.Set("err", fmt.Errorf("failed to get checksum for version %s: %w", updateVersion, err))
			_ = os.Remove(apps.UpdateImageFile)
			return
		}
		defer hashResp.Body.Close()

		checksumRaw, err := io.ReadAll(hashResp.Body)
		if err != nil {
			ctx.Set("err", fmt.Errorf("failed to get checksum for version %s: %w", updateVersion, err))
			_ = os.Remove(apps.UpdateImageFile)
			return
		}
		checksumSrc := strings.Split(string(checksumRaw), " ")[0]

		if checksumFile != checksumSrc {
			ctx.Set("err", fmt.Errorf("checksum mismatch: expected %s got %s", checksumSrc, checksumFile))
			_ = os.Remove(apps.UpdateImageFile)
			return
		}

		_ = ioutil.WriteFile(apps.UpdateImageNameFile, []byte(path.Base(updateUrl)), os.ModePerm)

		// TODO sync multiple requests
		ctx.Redirect(http.StatusFound, "/update/confirm")
		ctx.Abort()
	}
}

func getUpdateConfirm(ctx *gin.Context) {
	ctx.Set("title", "Confirm Update")

	if _, err := os.Stat(apps.UpdateImageFile); os.IsNotExist(err) {
		ctx.Set("status", http.StatusNotFound)
		return
	}

	fileNameRaw, err := ioutil.ReadFile(apps.UpdateImageNameFile)
	if err != nil {
		ctx.Set("status", http.StatusNotFound)
		return
	}

	ctx.Set("tpl", "update_confirm")

	hasher := sha256.New()
	imgFile, err := os.Open(apps.UpdateImageFile)
	if err != nil {
		ctx.Set("err", fmt.Errorf("failed to open image file: %w", err))
	} else {
		_, err = io.Copy(hasher, imgFile)
		if err != nil {
			ctx.Set("err", fmt.Errorf("failed generate checksum: %w", err))
		}
	}

	vars := ctx.MustGet("vars").(map[string]interface{})
	vars["Checksum"] = hex.EncodeToString(hasher.Sum(nil))
	vars["ImageName"] = string(fileNameRaw)
}

func postUpdateConfirm(ctx *gin.Context) {
	err := alpine_builder.UpdateSystem(apps.UpdateImageFile)
	if err != nil {
		ctx.Set("err", fmt.Errorf("failed to update system: %w", err))
		return
	}

	// update successful -> do reboot
	ctx.Redirect(http.StatusFound, "/system/reboot")
	ctx.Abort()

	go func() {
		time.Sleep(time.Millisecond * 500)
		err := alpine_builder.SystemReboot()
		if err != nil {
			log.Printf("reboot failed: %v", err)
		}
	}()
}
