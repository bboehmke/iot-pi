package web

import (
	"embed"
	"encoding/base64"
	"fmt"
	"html/template"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	go_version "github.com/hashicorp/go-version"
	"github.com/xanzy/go-gitlab"
	"gitlab.com/bboehmke/iot-pi/version"
	alpine_builder "gitlab.com/raspi-alpine/go-raspi-alpine"
)

//go:embed templates/*
var assets embed.FS

// Tpl contains templates for web pages
var Tpl = template.Must(template.ParseFS(assets, "templates/*"))

// render the current request from template
func render(ctx *gin.Context) {
	ctx.Set("vars", make(map[string]interface{}))
	ctx.Set("title", "")
	ctx.Set("tpl", "404")
	ctx.Set("status", http.StatusOK)
	ctx.Set("err", nil)

	ctx.Next()

	if ctx.IsAborted() {
		return
	}

	vars := ctx.MustGet("vars").(map[string]interface{})
	vars["PageTitle"] = ctx.MustGet("title")
	vars["Err"] = ctx.MustGet("err")
	vars["Version"] = version.Version

	ctx.HTML(ctx.GetInt("status"), ctx.GetString("tpl")+".gohtml", vars)
}

// simpleTpl handler for get requests
func simpleTpl(title, tpl string) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		ctx.Set("title", title)
		ctx.Set("tpl", tpl)
	}
}

// BasicAuth middleware for root user
func BasicAuth(ctx *gin.Context) {
	realm := "Basic realm=IoT-Pi"

	// check if auth header was set
	auth := strings.SplitN(ctx.GetHeader("Authorization"), " ", 2)
	if len(auth) != 2 || auth[0] != "Basic" {
		ctx.Header("WWW-Authenticate", realm)
		ctx.AbortWithStatus(http.StatusUnauthorized)
		return
	}

	// get username and password from auth head
	payload, _ := base64.StdEncoding.DecodeString(auth[1])
	pair := strings.SplitN(string(payload), ":", 2)

	if len(pair) != 2 || pair[0] != "root" {
		ctx.Header("WWW-Authenticate", realm)
		ctx.AbortWithStatus(http.StatusUnauthorized)
		return
	}

	// check if root password matched
	match, _ := alpine_builder.SystemCheckRootPassword(pair[1])
	if !match {
		ctx.Header("WWW-Authenticate", realm)
		ctx.AbortWithStatus(http.StatusUnauthorized)
	}
}

// getAvailableUpdate from gitlab project
func getAvailableUpdate() (string, string, error) {
	gl, _ := gitlab.NewClient("")

	releases, _, err := gl.Releases.ListReleases(32369587, nil)
	if err != nil {
		return "", "", fmt.Errorf("failed to get updates from project page: %w", err)
	} else {
		var updateVersion, updateUrl string
		lastVersion, _ := go_version.NewVersion("0.0.0")

		for _, rel := range releases {
			var url string
			for _, link := range rel.Assets.Links {
				if strings.HasSuffix(link.Name, version.Arch+"_update.img.gz") {
					url = link.URL
				}
			}
			if url != "" {
				v, err := go_version.NewVersion(rel.TagName)
				if err == nil {
					if v.GreaterThan(lastVersion) {
						lastVersion = v
						updateUrl = url
						updateVersion = rel.TagName
					}
				}
			}
		}

		return updateVersion, updateUrl, nil
	}
}
