package web

import (
	"errors"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	alpine_builder "gitlab.com/raspi-alpine/go-raspi-alpine"
)

func RegisterSystem(router gin.IRouter) {
	group := router.Group("/system", render)

	group.GET("/", getSystem)
	group.POST("/", postSystem, getSystem)
	group.GET("/reboot", simpleTpl("System Reboot", "system_reboot"))
}

func getSystem(ctx *gin.Context) {
	ctx.Set("title", "System Settings")
	ctx.Set("tpl", "system")

	networkInfo, err := alpine_builder.GetNetworkInfo()
	if err != nil {
		ctx.Set("err", fmt.Errorf("failed to get network info: %w", err))
		networkInfo = new(alpine_builder.NetworkInfo)
	}

	sshEnabled, err := alpine_builder.SystemSSHEnabled()
	if err != nil {
		ctx.Set("err", fmt.Errorf("failed to get SSH state: %w", err))
	}

	timeZones, err := alpine_builder.SystemListTimeZones()
	if err != nil {
		ctx.Set("err", fmt.Errorf("failed to get time zones: %w", err))
	}

	activeTimeZone, err := alpine_builder.SystemGetTimeZone()
	if err != nil {
		ctx.Set("err", fmt.Errorf("failed to get active time zone: %w", err))
	}

	vars := ctx.MustGet("vars").(map[string]interface{})
	vars["Network"] = networkInfo
	vars["SshEnabled"] = sshEnabled
	vars["TimeZones"] = timeZones
	vars["ActiveTimeZone"] = activeTimeZone
}

func postSystem(ctx *gin.Context) {
	switch ctx.PostForm("action") {
	case "update_network":
		if ctx.PostForm("dhcp") != "" {
			ctx.Set("err", alpine_builder.NetworkEnableDHCP())
		} else {
			ctx.Set("err", alpine_builder.NetworkSetStatic(
				ctx.PostForm("address"),
				ctx.PostForm("netmask"),
				ctx.PostForm("gateway")))
		}
	case "update_password":
		if ctx.PostForm("password1") != ctx.PostForm("password2") {
			ctx.Set("err", errors.New("passwords does not match"))
		} else {
			ctx.Set("err", alpine_builder.SystemSetRootPassword(ctx.PostForm("password1")))
		}
	case "update_ssh":
		if ctx.PostForm("enabled") != "" {
			ctx.Set("err", alpine_builder.SystemEnableSSH())
		} else {
			ctx.Set("err", alpine_builder.SystemDisableSSH())
		}
	case "update_time_zone":
		ctx.Set("err", alpine_builder.SystemSetTimeZone(ctx.PostForm("time_zone")))

	case "shutdown":
		ctx.Set("err", alpine_builder.SystemShutdown())

	case "reboot":
		ctx.Redirect(http.StatusFound, "/system/reboot")
		ctx.Abort()

		go func() {
			time.Sleep(time.Millisecond * 500)
			err := alpine_builder.SystemReboot()
			if err != nil {
				log.Printf("reboot failed: %v", err)
			}
		}()
	}
}
