package web

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/bboehmke/iot-pi/apps"
)

func RegisterApps(router gin.IRouter) {
	group := router.Group("/apps", render)

	group.GET("/", getApps)
	group.GET("/:app", getApp)
	group.POST("/:app", postApp)
	group.GET("/:app/example_conf/:conf", getConf)
}

func getApps(ctx *gin.Context) {
	ctx.Set("title", "Apps")
	ctx.Set("tpl", "apps")

	vars := ctx.MustGet("vars").(map[string]interface{})
	vars["Apps"] = apps.Apps.Apps()
}

func getApp(ctx *gin.Context) {
	app := apps.Apps.GetApp(ctx.Param("app"))
	if app == nil {
		ctx.Set("title", "App")
		ctx.Set("status", http.StatusNotFound)
	} else {
		ctx.Set("title", app.Def.Meta.Name)
		ctx.Set("tpl", "app")

		vars := ctx.MustGet("vars").(map[string]interface{})
		vars["App"] = app
	}
}

func postApp(ctx *gin.Context) {
	app := apps.Apps.GetApp(ctx.Param("app"))
	if app == nil {
		ctx.Set("title", "App")
		ctx.Set("status", http.StatusNotFound)
		return
	}
	ctx.Set("title", app.Def.Meta.Name)
	ctx.Set("tpl", "app")

	vars := ctx.MustGet("vars").(map[string]interface{})
	vars["App"] = app

	switch ctx.PostForm("action") {
	case "start":
		ctx.Set("err", app.Start())
	case "stop":
		ctx.Set("err", app.Stop())
	case "enable_autostart":
		ctx.Set("err", apps.Apps.SetAutostart(app.Def.Meta.Key, true))
	case "disable_autostart":
		ctx.Set("err", apps.Apps.SetAutostart(app.Def.Meta.Key, false))
	case "update_config":
		conf, ok := app.ConfigFiles[ctx.PostForm("config_key")]
		if !ok {
			ctx.Set("err", fmt.Errorf("unknown config: %s", ctx.PostForm("config_key")))
		} else {
			ctx.Set("err", conf.Write(ctx.PostForm("config")))
		}
	default:
		ctx.Set("err", fmt.Errorf("unknown action: %s", ctx.PostForm("action")))
	}
}

func getConf(ctx *gin.Context) {
	app := apps.Apps.GetApp(ctx.Param("app"))
	if app == nil {
		ctx.Set("title", "Example Conf")
		ctx.Set("status", http.StatusNotFound)
	} else {
		conf, ok := app.ConfigFiles[ctx.Param("conf")]
		if !ok {
			ctx.Set("title", "Example Conf")
			ctx.Set("status", http.StatusNotFound)
		} else {
			ctx.String(http.StatusOK, conf.Example)
			ctx.Abort()
		}
	}
}
