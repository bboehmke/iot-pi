package version

// Version of application (set on build)
var Version = "0.0.0"

// Arch contains the architecture of the application
var Arch = "armv7"
