#!/sbin/openrc-run

command="/usr/bin/iot-pi"
pidfile="/var/run/iot-pi.pid"
command_args="-reset-boot-counter -enable-auth -listen-addr :80"
command_background=true


depend() {
	use logger dns
	need net
	after firewall
}
